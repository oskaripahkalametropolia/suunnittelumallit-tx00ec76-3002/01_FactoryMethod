package factorymethod;

public class Opiskelija extends AterioivaOtus {

    public Juoma createJuoma(){
        return new Olut();
    }

    public Ruoka createRuoka(){
        return new Pitsa();
    }
}