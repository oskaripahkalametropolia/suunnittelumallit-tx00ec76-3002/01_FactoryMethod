package factorymethod;

public class Lapsi extends AterioivaOtus{

    @Override
    public Juoma createJuoma() {
        return new Mehu();
    }

    @Override
    public Ruoka createRuoka() {
        return new Kalapuikot();
    }


}
