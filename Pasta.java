package factorymethod;

public class Pasta implements Ruoka {
    @Override
    public String toString() {
        return "pasta";
    }
}
